def possible?(n)
  n.to_s != n.to_s.chars.sort.reverse.join
end

# puts possible?(4321) # false
# puts possible?(4231) # true

def next_bigger_helper(n)
  if possible?(n)
    while true do
      r = n.to_s.chars.shuffle.join.to_i
      return r if r > n 
    end  
  end
end

def next_bigger(n)
  res = []
  1.upto(Math.sqrt(n).to_i) do
    res.push(next_bigger_helper(n))
  end
  res.min
  res.tap {|o| p o}
end

puts next_bigger(10000)

# the bogo approach is fucking stupid
# fml
