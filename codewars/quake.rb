def strong_enough( earthquake, age )
  safe = "Safe!"
  not_safe = "Needs Reinforcement!"
  house = 1000
  age.times {|| house = house / 1.01} 
  quake = earthquake.map{ |sa| sa.inject(:+) }.inject(:*).to_f
  # age the house, measure the quake, and compare. ezpz
  if (house >= quake)
    safe
  else 
    not_safe
  end
end