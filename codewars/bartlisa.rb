def list(names)
  res = [] # puts names here first, simplify
  names.each do |namehash|
    res.push(namehash[:name])
  end
  
  case res.size
  when 2 
    "#{res[0]} & #{res[1]}"
  when 1
    "#{res[0]}"
  else
    res[0..-3].join(', ') + ", #{res[-2]} & #{res[-1]}"
  end
end