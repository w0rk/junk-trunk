def squareSum(numbers)
  sq = lambda { |n| n ** 2}
  numbers.map(&sq).inject(:+)
end