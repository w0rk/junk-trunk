def self_descriptive(num)
  num.to_s.split('').collect.with_index do |e, idx|
  num.to_s.count(idx.to_s).to_s == e
  end.count(false) == 0
end

p self_descriptive(21200)  # true, omfg this was wrong before
p self_descriptive(1210)   # true
p self_descriptive(2020)   # true, FAK 
p self_descriptive(11201)  # false
