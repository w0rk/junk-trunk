def double_check(str)
  not str.split('').each_cons(2).select { |a, b| a.upcase == b.upcase }.empty?
end
