def accum(s)
	res = []
  letters = s.split('')
  letters.length.times { |i| res.push((letters[i] * (i + 1)).capitalize) }
  res.join("-")
end