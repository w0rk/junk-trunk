def reverse_fun(n)
  len = n.length
  0.upto(len) do |i|
    case i
    when 0
      n = n.reverse
    else
      n = "#{n[0..i-1]}" + "#{n[i..-1].reverse}"
    end
  end
  n
end

# 0123456789 -> 9081726354

puts reverse_fun('01233456789')