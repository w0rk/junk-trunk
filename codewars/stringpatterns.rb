def pattern(n)
  res = []
  1.upto(n) do |i| 
    res.push(i.to_s * i)
  end
  res.join("\n")
end

puts pattern(10)