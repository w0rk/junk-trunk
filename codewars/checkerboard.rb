# i'll come back to this, w/e

def checkered_board(dimension)
  res = []
  
  squares = ["\u25A1","\u25A0"].cycle
  
  1.upto(dimension ** 2) do |i|
    i % dimension == 0 ? res.push("#{squares.next}\n") : res.push("#{squares.next} ")
  end
  
  res.join
  
end

puts checkered_board(5)
