def difference_of_squares(x)
  sq = lambda {|n| n ** 2}
  sq.call((1..x).inject(:+)) - (1..x).map(&sq).inject(:+)
end

puts difference_of_squares(10) # -> 2640