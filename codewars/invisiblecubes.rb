def not_visible_cubes(n)
  case n
  when 0
    0
  when 1
    0
  when 2
    0
  when 3
    1
  when 4
    8
  else 
    (n - 2) ** 3
  end
end