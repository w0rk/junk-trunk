def vowel_2_index(string)
  res = ""
  string.chars.each_with_index { |c, i| if "aeiouAEIOU".include?(c) then res << (i + 1).to_s else res << c end }
  res
end

puts vowel_2_index('Tomorrow is going to be raining')

# should somehow be T2m4rr7w 10s g1415ng t20 b23 r2627n29ng

# this shit was harder than it looks
