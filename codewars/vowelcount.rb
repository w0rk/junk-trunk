def getCount(inputStr)
  vowels = 'aeiou'.split('')
  inputStr.split('').keep_if { |char| vowels.member?(char) }.length
end