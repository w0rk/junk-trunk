#!/usr/bin/env lua

-- hayden jones 
-- for when you want to dump everything in _G
-- in the future I'd like this to be a repl-type thing
-- where you can explore tables better
-- i mean im not writing that shit today, but it'd be nice

local junk = require 'junk'

local function aux(k,v)
  if type(v) == 'table' then return true
  else return false end
end

local function xua(k,v)
  if type(v) == 'table' then return false
  else return true end
end

t1 = junk.filter(_G, aux)
t2 = junk.filter(_G, xua)

junk.pp(t1)
junk.pp(t2)
