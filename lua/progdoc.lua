-- Q:
-- is programmatic documentation possible in lua through string literals?
-- A:
-- uh, idunno
-- A2:
-- yeah kinda
mt = {__call = function (caller, whom) print("hello " .. whom) end}

hello = {}

hello.doc = "this is a string"

setmetatable(hello, mt)

hello("hayden")
print(hello.doc)
