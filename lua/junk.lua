#!/usr/bin/env lua

-- junk.lua
-- current status: everything -> WORKS
-- times this has been fucked with: 8
-- increment this to keep track ^ ^ ^
-- this is my masterpiece
-- how sad

junk = {}

function junk.pp(t)
  -- how many times have I written this? let me count the dozens

  for k,v in pairs(t) do print(k,v) end
end

function junk.make_serial(start, interval)
  -- closures are cool, eh?

  count    = start    or 0
  interval = interval or 1

  -- closure here, isn't that neat?
  -- the returned function retains closures of start and interval
  -- and you can have different serial generators

  return function ()
      count = count + interval
      return count
    end

  -- to increment the counter, call it because it doesn't retain state other
  -- than that closure and there's not a way to read that without calling it
  -- as far as I know, maybe more on this later

end

function junk.fizzbuzz(max) -- for good measure, b.t.h.i.a.n.n.i.t.t.n.i.a.n.h.i.
  -- surely I don't need to explain fizzbuzz, right?

  local res = {}
  for i=1,max do
    if math.fmod(i,15) == 0 then res[i] = "fizzbuzz"
    elseif math.fmod(i,5) == 0 and not (math.fmod(i,3) == 0) then res[i] = "buzz"
    elseif math.fmod(i,3) == 0 and not (math.fmod(i,5) == 0) then res[i] = "fizz"
    else -- nah, yo
    end
  end
  return res
end

function junk.take(t,pos,len)
  -- this returns a table formed from table t
  -- if len is not given, it's basically nthcdr
  -- otherwise it just takes that many elements

  local res = {}
  len = len or #t -- number of elements or collect all, nils disappear
  pos = pos or 1 -- might as well
  for i=1,len do res[i] = t[pos + i - 1] end
  return res
end

function junk.take_cons(t,sz)
  -- this is similar to .each_cons in ruby
  -- it returns a table of tables, each with a slice of sz size
  -- so (1,2,3,4) could be ((1,2)(2,3)(3,4)) etc

  local res = {}
  for i = 1,(#t-sz) do
    res[i] = junk.take(t,i,sz)
    end
  return res
end

function junk.filter(t,f)
  -- this returns a table that is the result of the TRUE results
  -- of doing f(k,v) on every pair in t
  -- the function you pass in needs to handle both k and v, obviously

  local res = {}
  for k,v in pairs(t) do
    if f(k,v) then res[k] = v end
  end
  return res
end

--[[

nums = {1,3,5,7,8}

function helper(k,v)
  -- dont need k here
  if v > 5 then
   return true
   else return false
  end
end

junk.pp(junk.filter(nums, helper))
-- 7, 8
--]]

function junk.tlen(t, excludes)
  -- this retuns a length of t, but doesnt count any items
  -- that are either nil or present in the exclusion table
  -- if an exlcusion table isnt provided then it returns
  -- a count of non-nil elements

  local res = 0
  excludes = excludes or {}
    if t == nil then return nil
    end
  for k,v in pairs(t) do
    if v ~= nil and not excludes[v] then res = res + 1 end
  end
  return res
end

--[[

example = {1,2,3,4,5,6,7}
excl    =     {3,4,5}

print(junk.tlen(example, excl))
-- prints 4
--]]

function junk.explode_string(str)
  -- this takes a string and returns a table where each index is
  -- each byte of the string, for easier indexing 
  -- to avoid getting WET
  
  local res = {}
  for i=1, string.len(str) do
    res[i] = string.char(string.byte(str, i))
  end
  return res
end

function junk.table_equal(t1, t2)
  -- tests to make sure that both tables have the same keys
  -- somehow this works if either table is asymmetrical

  local res = 0 
  for k,v in pairs(t1) do
    if t1[k] == t2[k] then res = res + 1 end
  end
  if res >= #t1 and res >= #t2 then 
    return true else 
    return false 
  end
end

function junk.start_with(target, searchfor) -- target can be string or table full of strings
  -- this tests whether target starts with the searchfor string
  -- so {'a','b','r','a','h','a','m'} does indeed start with 'ab'

  local work
  if type(target) == 'table' then do work = table.concat(target) end
  else work = target end
  if string.find(work, searchfor) == 1 then return true else 
    return false
  end
end

return junk -- EOF here, no trespassing
