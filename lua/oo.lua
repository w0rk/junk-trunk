#!/usr/bin/env lua

-- hayden jones
-- jk this is all stolen from PIL
-- just helps to understand it if you actually type it out

local junk = require('junk')

account = {balance = 0}

function account:new(fields)
  fields = fields or {balance = 0}
  self.__index = self -- read explanation
  return setmetatable(fields, self)
end

--[[ explanation of 'self.__index = self'
this is the line that makes this whole thing work
so if we had like:

admin_acct = account:new

that means that admin_acct.__index is account (!)
so we could override account:new by defining admin_acct:new
and any other methods that conflict will get overridden, since
__index is checked if the key ISNT found somewhere.
--]]

function account:deposit(amount)
  if amount and amount > 0 then
    self.balance = self.balance + amount
  end
end

a1 = account:new{balance = 100}
junk.pp(a1)
a1:deposit(500)
junk.pp(a1)

