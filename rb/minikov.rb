#!/usr/bin/env ruby
# encoding: UTF-8

# current status: not broken

# Minikov is a project that I started a long time ago to generate spam that
# would be capable of defeating bayesian filters. Obviously this project is
# a long way off from doing that, but I continue to work on it.
#
# I have a file called pg.txt that I pass to this thing to test it, that
# contains text from Paul Graham's essays. His tone is somewhat consistent
# and he talks about a wide variety of stuff. It's only a 2MB file, but in
# previous renditions of minikov, it's been able to generate sentences that
# almost make sense. This project originally was written in Common Lisp, then
# Racket, then Guile, then Lua, and now Ruby. The racket one got the farthest,
# but this Ruby version is going to go the distance also.

# Anyways, thanks for reading this if you actually did, let me know if you
# need a feature implemented or something.

# this is a class documentation comment to shut rubocop up
class Minikov
  WORD_SPLIT = /[\w'.,!?]+/

  attr_accessor :words

  def initialize(file)
    # just tell Minikov where the file is at, it'll do the rest
    @words = File.read(file, encoding: Encoding::UTF_8).scan(WORD_SPLIT)
  end

  def pairs_that_start_with(start_with)
    # this returns an array of all pairs where the first word is start_with
    @words.each_cons(2).select { |cons| cons[0] == start_with }
  end

  def words_after_frag(frag)
    # returns all words that were found directly after all occurences of frag
    @words.each_cons(work.length + 1).select { |cons| cons[0..-2] == frag.scan(WORD_SPLIT) }
  end

  def n_words_after_frag(n, frag)
    # returns an array of arrays, each subarray being of length n, where the
    # first words are equal to the frag passed in
    work = frag.scan(WORD_SPLIT)
    @words.each_cons(n).select { |cons| cons.first(work.length) == work }
  end

  def words_after(word)
    # returns an array of all words that have occurred after word
    @words.each_cons(2).select { |cons| cons[0] == word }
      .uniq
      .each_with_object([]) { |each, res| res.push(each[1]) }
  end

  def words_before(word)
    # returns an array of all words that have occurred before word
    @words.each_cons(2).select { |cons| cons[1] == word }
      .uniq
      .each_with_object([]) { |each, res| res.push(each[0]) }
  end

  def all_frags_where(n, &block)
    # block recieves fragment as ['an', 'array', 'of', 'words', 'like', 'this']
    # and each cons of size n has &block called, and the returned array
    # is an array of arrays of frags that returned true from the block
    @words.each_cons(n).select(&block)
  end

  def get_prob_for_pair(first_word, second_word)
    # return a float between 0 and 1 that represents the probability
    # that the second word occurs directly after the first word, based
    # on the source words
    all_frags_where(2) do |frag|
      frag[0] == first_word && frag[1] == second_word
    end.length.to_f / @words.count(first_word).to_f
  end

  def frag_pairs_probs(frag)
    # this returns a hash of words, with each key being a word and
    # each value representing the probability that the word after the word
    # will occur
    res = {}
    frag.scan(WORD_SPLIT).each_cons(2) do |a, b|
      res[a] = get_prob_for_pair(a, b)
    end
    res
  end
end # class Minikov

# TODO: determine how to generate sentences
# the hard way: start word, add words until ending-punctuation, drop if weight
# is too far

# another way: take existing sentences and morph words in it until
# it's different and the weight is in check. possibly non-terminating
# though, we'll have to have a maximum number of morph tries

mk = Minikov.new(ARGV[0])
# p mk.words
# p mk.pairs_that_start_with('I')
# p mk.words_after_frag('I')
# p mk.n_words_after_frag(5, 'I just')
# p mk.words_after('language')
# p mk.words_before('money')
# p mk.all_frags_where(2) { |frag| frag[0] == 'Common'}.uniq
# p mk.get_prob_for_pair('Common', 'Lisp')
# p mk.n_words_from_fragment(4, 'Common Lisp is')
# p mk.frag_pairs_probs('spent money')
