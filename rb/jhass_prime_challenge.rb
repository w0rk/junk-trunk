require 'prime'
PRIMES = (1..Float::INFINITY).lazy.select(&:prime?)

# this is awaiting approval from jhass directly

def make_prime_sayer(starting_time)
  dt = starting_time
  c = 0
  return lambda do ||
    begin
      c += 1 if (Time.new.to_i - dt) >= 5
      p PRIMES.next if (Time.new.to_i - dt) >= 5
      dt = Time.new.to_i if (Time.new.to_i - dt) >= 5
    end
  end
end

ps = make_prime_sayer(Time.new.to_i)

while true do ps.call end

# whatever, jhass

# the point of this was to print a prime number but to take five seconds
# the idea was that you would compute it very slowly
# you cant use sleep

# my thought was to attach a block to time so that it ran each second
# and then just print a prime on every second divisible by five

# harder than it sounds without using sleep

# i feel like a pussy for not having ever completed this
# and i wonder...
# what if I just had a busy loop with a counter that counted
# how long it had been since it last yielded to some &block?


