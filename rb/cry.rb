#!/usr/bin/env ruby
# the cry class makes ruby cry, cycles just going to waste!

# yes this is stupid, I agree.
# fbs is almost worse

class Cry
  def initialize(pool)
    @data = pool.split('')
  end

  def take(num)
    res = ''
    num.times do
      res.concat(@data.sample)
    end
    res
  end
end

def bogo(length, interval = 50_000)
  c = Cry.new('0123456789abcdefghijklmnopqrstuvwxyz')
  d = Cry.new('0123456789abcdefghijklmnopqrstuvwxyz')
  tries = 0
  match = c.take(length)
  while (d.take(length) != match)
    tries += (1)
    puts tries / interval if tries.modulo(interval) == 0
  end
end

bogo(7, 10_000_000)
