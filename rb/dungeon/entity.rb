#!/usr/bin/env ruby
# MIT LICENSE
# k0an 2015

# an entity is a player or monster in the dungeon
class Entity
  attr_accessor :stats
  def initialize(max_stat, tile)
    @stats = {}
    @location = tile
    # set the location of the Entity object to that of the tile
    # object. 
    [:hp, :str, :res].each { |s| @stats[s] = Random.rand(max_stat) + 1 }
    # initialize stats with random values
  end
end # class Entity
