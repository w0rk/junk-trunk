#!/usr/bin/env ruby
# MIT LICENSE
# crime 2015

# i hate this project. everything in here needs to be gutted and rewritten

require_relative 'hero'

# the dungeon has an array that holds entities
class Dungeon
  attr_accessor :entities
  def initialize
    @entities = {}
  end

  def process_attack(attacker, defender)
    stat_diff = attacker.stats[:str] - defender.stats[:res]
    defender.stats[:hp] -= stat_diff if stat_diff > 0
    entities.delete_if { |_k, v| v.is_a?(Entity) && v.stats[:hp] <= 0 } if stat_diff > 0
  end
end # class Dungeon

dungeon = Dungeon.new

dungeon.entities[:hero] = Hero.new('crime', 20)
dungeon.entities[:monster] = Entity.new(5)

p dungeon.entities
puts dungeon.entities.length
dungeon.process_attack(dungeon.entities[:hero], dungeon.entities[:monster])
p dungeon.entities
puts dungeon.entities.length

# line 17 has added a new item to entities instead of nil'ing the one
# I was trying to refer to?
