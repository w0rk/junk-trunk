#!/usr/bin/env ruby
# MIT LICENSE
# k0an 2015

require_relative 'entity'

# our valiant hero
class Hero < Entity
  attr_reader :name
  def initialize(name, max_stat)
    @name = name
    super(max_stat)
  end
end # class Hero
