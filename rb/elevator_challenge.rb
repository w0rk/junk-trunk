#!/usr/bin/env ruby

# elevator challenge

# so you think you can program an elevator?

# dont have the official spec rn, but it goes like this:
# the elevator moves a single floor at a time and moves in one direction only
# meaning if it gets a call on the bottom floor when its heading up, it will
# not go back down until it has served its highest call, and it will serve every
# call on the way down as long as its on the way. there was something else but
# im sure this will be a fiasco of a class already, so let's just get on with it

# you might think: hey thats easy. right?

# well fuck you, write it your damn self then

# my design idea: order objects talk to elevator objects
# elevator objects dont represent floors directly, instead moving up or down a
# floor is just changing a current_floor variable, and asking if the elevator
# has orders on that floor. elevators will remove orders right before they leave the floor

# call should be renamed to an order because Call != .call and other stuff

# fuck a bufd though, I haven't actually read POODR yet because I'm lazy, tho

class Order
  attr_reader :location, :where_to, :direction
  def initialize(location, where_to, direction)
    @location = location
    @where_to = where_to
    @direction = direction if direction == :up || :down
  end
end # class Order

class Elevator
  attr_accessor :floors, :direction, :location
  def initialize(floors, location = 0, direction = :up)
    @floors = Array.new(how_many_floors)
    @location = location
    @direction = direction
  end
  
  def serve_floor
    puts 'Served floor #{location'
    @floors[location] = nil # everybody gets off at their stop
  end
  
  def check_floor
    serve_floor if @floors[location].any? { |obj| obj.is_a?(Order) }
  end
  
  def turn_around?
    # the idea here is that @floors contains orders, and we check to see if
    # the elevator's location is such that the elevator needs change 
    # according to what the location of the topmost or bottommost order's
    # location attribute is. otherwise, this does nothing, and the elevator
    # should move on to the next floor after serving the floor.
    
    case @direction
      when :up
        @direction = :down if @location >= @floors.select { |ord| ord != nil }.last.location
      when :down
        @direction = :up if @location <= @floors.select { |ord| ord != nil}.first.location
    end
  end
  
  # TODO: the elevator needs to know how to turn around
  # easier said than done, let me tell ya
  
  # TODO: this whole thing needs to be reworked. I've realized that there is
  # no distinction between orders and people actually on the damn elevator,
  # so what serve floor actually does is let people get on the elevator and 
  # then promptly serve the order, leaving the passenger on the elevator for
  # the ride without UGGHHHHHHHH
  
  # a better way would be iterate through elevator passengers.
  # the elevator's turnaround logic could stay the same I think, since if an
  # order comes from above the elevator when it's going down, the elevator
  # will not turn around yet.
  # the elevator should also always change direction at both the topmost
  # and bottommost floors, obviously.
  
  # this is a lot harder than I initially suspected it would be.
  
end # class Elevator
