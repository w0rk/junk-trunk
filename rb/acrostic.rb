#!/usr/bin/env ruby

def acrostic(str)
  str.split(' ').map { |s| s[0] }.join
end

p acrostic('hayden is cool, can u please')
