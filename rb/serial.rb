#!/usr/bin/env ruby

# makes a serial object, get is a singleton method
class MakeSerial
  def initialize(start = 0, incr = 1)
    @count = start
    @incr = incr
    def self.get
      @count += @incr
      @count
    end
  end
end
