def gauss(n)
  (1..n).inject(:+)
end

0.step(99999, 99) { |n| puts "n: #{n} gauss(n): #{gauss(n)}" }
