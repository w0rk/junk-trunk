# can ruby into /prog/'s fibonacci butt sort?
# this sort of works but I'm not sure it's operating on the right indices
# luckily I seem to have misplaced all the shits I was going to give
# it was sort of fun to write, I guess

# ps. this does not deserve to pass rubocop, and I'm not touching this ever again anyways

$fib = Hash.new do |h, k|
  h[k] = $fib[k-1] + $fib[k-2]
end.update(0 => 0, 1 => 1)

# global for quality assurance
# which is to assure you that there is no quality herein and henceforth

def slice_fib(range)
  range.each_with_object([]){ |e, o| o.push $fib[e] }
end

# a little jeg magic there

def buttsort(target_butt, &block) # your butt should be an array
  raise 'wrong butt' unless target_butt.is_a?(Array) # ENTERPRISE QUALITY CODE
  target_butt.each_with_index do |e, i|
    block.call(e) if slice_fib(0..target_butt.length).member?(i)
    # this is a mess but let me explain, take a range of fibs from 0 to target.length
    # if the index of each thing in target_butt can be found in fibs, then we call the block
    # which in most use cases would mutate whatever it is in place
    # it is worth noting that the fibonacci buttsort by spec does not sort anything
    # and could be more accurately named fibonacci butt-index-mapper
    # fuckin' /prog/
  end
end

p buttsort("fibonacci butt sort is the greatest thing to come from /prog/ since redcream".split("")) {|e| e.upcase!}.join
