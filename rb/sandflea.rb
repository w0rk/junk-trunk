# sandflea
# reversible cellular automata
# hayden jones, homeless, please hire me
# MIT License 2015

# this class is meant to make a cellular automata
# based on the array of phases that you pass in
# the phases should already be in order and should
# be toroidal in that the next phase from the last
# one in the array should be the first one in the array
class Sandflea
  attr_accessor :phases
  attr_reader :statesize

  def initialize(phases)
    @phases = phases.cycle # an enumerator, which means fun
    @statesize = phases.length
  end

  # i chose not to use cycle because it's easier to write prev_phase
  # jk lets do it (rewritten)

  def next_phase(item)
    case @phases.find_index(item)
    when @statesize - 1
      @phases.rewind.take(@statesize).to_a[0]
    else
      @phases.rewind.take(@statesize).to_a[@phases.find_index(item) + 1]
    end
  end

  def prev_phase(item)
    case @phases.find_index(item)
    when 0
      @phases.rewind.take(@statesize).to_a[-1]
    else
      @phases.rewind.take(@statesize).to_a[@phases.find_index(item) - 1]
    end
  end

  def self.advance_by(world:, flea:, by:) # a class method
    by.times { world.map! { |ea| ea.map! { |atom| flea.next_phase(atom) } } }
  end
end # class Sandflea

world = [[1, 2, 3, 4], [2, 3, 4, 1], [3, 4, 1, 2], [4, 1, 2, 3]]
world_flea = Sandflea.new([1, 2, 3, 4])
Sandflea.advance_by(world: world, flea: world_flea, by: 5)
p world

# world_flea = Sandflea.new([1,2,3,4])
# world = [[1,2,3,4],[2,3,4,1],[3,4,1,2],[4,1,2,3]]

# p world

# world.map! do |ea|
#   ea.map! do |atom|
#     world_flea.next_phase(atom)
#   end
# end

# p world

# flea = Sandflea.new([1,2,3,4])
# p flea.next_phase(3) # 4
# p flea.next_phase(4) # 1
# p flea.prev_phase(3) # 2
# p flea.prev_phase(1) # 4
