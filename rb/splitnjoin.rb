# whats the point of this

def splitnjoin(str, spl)
  res = str.split(spl)
  res.map do |s|
    if s === res.last then s
    else s + spl
    end
  end
end

sentence = "hayden is such a cool guy"

# we want
# ["hayden ", "is ", "such ", "a ", "cool ", "guy"]

p splitnjoin(sentence, " ")
