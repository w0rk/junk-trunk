import 'dart:io';
import 'dart:async';

void main() {
  String f = new File('/home/hayden/journal.txt').readAsStringSync();
  RegExp wordSplit = new RegExp(r"(\w+)");
  Iterable<Match> words = wordSplit.allMatches(f);
  words.forEach((e) {
    //String m = e.group(0);
    //print(m);
  });
}

class WordGrabber(String path_to_file, fn) {
  RegExp rgx = new RegExp(r"(\w+)");
  String file = new File(path_to_file).readAsStringSync();
  Iterable<Match> words = rgx.allMatches(file);
  words.forEach((e) {
    fn(e);
  });
  WordGrabber(this.path_to_file, this.fn);
}
// i hate dart's syntax. ruby syntax is so much cleaner
// implicit method definitions? are you kidding me??
